#! /usr/bin/env python3

from argparse import ArgumentParser
import http.server
import os
import shutil
import socketserver

class MyHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        """Serve a GET request."""
        self.send_response(200)
        self.send_header('Content-type', 'application/x-x509-user-cert')
        filestats = os.stat(args.certificate)
        self.send_header("Content-Length", str(filestats[6]))
        self.end_headers()
        certificate_file = open(args.certificate, 'rb')
        try:
            shutil.copyfileobj(certificate_file, self.wfile)
        finally:
            certificate_file.close()

parser = ArgumentParser()
parser.add_argument('-p', '--port', dest='port', default=8080, type=int, help='port to listen to (default: 8080)')
parser.add_argument('certificate', metavar='certificate_file', default='certificate.pem', help='certificate file to be served (default: certificate.pem)')
args = parser.parse_args()

Handler = MyHandler
httpd = socketserver.TCPServer(('localhost', args.port), Handler)
httpd.handle_request()
