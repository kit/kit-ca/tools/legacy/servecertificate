ServeCertificate
================

This is a minimal Python script designed to serve an X.509 certificate file
via HTTP, giving the MIME type of `application/x-x509-user-cert`.  This
allows Firefox users to import their own certificate and join it with its
corresponding private key if they only have access to the certificate in
file format after having requested it with Firefox.  In particular, this is
the case if a user requests a certificate of the KIT-CA (or, indeed, any
DFN-PKI CA), does not allow the certificate to be published, but then
forgets the access PIN.  This means the certificate cannot be downloaded
via the usual CA web interface because this operation requires the access
PIN.  The DFN will send the certificate to the user via mail upon creating
it, so the user does have the certificate in file format.  Unfortunately,
Firefox will not join a certificate with a private key if it is imported
from file, so this does not immediately help.

This script merely takes the certificate file and serves it once via HTTP
to port 8080, setting the MIME type to the appropriate
`application/x-x509-user-cert`.  This allows users to take their
certificate file and import it into their Firefox, joining it with the
corresponding private key, by launching the script with the certificate
file and then directing their Firefox to `http://localhost:8080`.

The port is set to 8080 by default, but can be changed with the `--port`
parameter.

Windows Binary
==============

A 32-bit stand-alone Windows binary with the script is provided in [the
release notes](https://git.scc.kit.edu/KIT-CA/ServeCertificate/tags).  To
use the Windows binary, simply drag and drop the certificate file onto the
executable file.  A shell window will pop up as the script is started.
Then, direct your Firefox to load `http://localhost:8080`.  After the
certificate has been served, the window will disappear, and the script will
terminate.

If you want to roll the Windows EXE file yourself, you will need a Windows
system with a working Python 3 installation and the `pyinstaller` and the
`pyopenssl` packages installed.  Running `Windows\roll_executable.bat` should
create a working binary in `dist\ServeCertificate.exe`.
